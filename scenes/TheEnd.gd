extends Control

onready var label : Label = $Label
onready var tween : Tween = $Tween

func _ready() -> void:
	var clear_time : float = round((Levels.end_time - Levels.start_time) / 1000.0)
	var clear_time_minutes : int = int(floor(clear_time / 60.0))
	var clear_time_seconds : int = int(clear_time) % 60
	label.text = "Your time was %d %s, %d %s.\r\nThank you for playing!" % [clear_time_minutes, "minute" if clear_time_minutes == 1 else "minutes", clear_time_seconds, "second" if clear_time_seconds == 1 else "seconds"]
	tween.interpolate_property(label, "modulate", Color.transparent, Color.white, 2.0, Tween.TRANS_LINEAR, Tween.EASE_OUT, 2.0)
	tween.interpolate_property(label, "modulate", Color.white, Color.transparent, 2.0, Tween.TRANS_LINEAR, Tween.EASE_OUT, 10.0)
	tween.start()
	yield(tween, "tween_all_completed")
	get_tree().change_scene("res://scenes/TitleScreen.tscn")

func _enter_tree() -> void:
	$Label.modulate = Color.transparent
