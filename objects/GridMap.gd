extends GridMap

func make_invisible() -> void:
	for block_name in ["InvisibleBlock"]:
		var block_index : int = mesh_library.find_item_by_name(block_name)
		var block_mesh : Mesh = mesh_library.get_item_mesh(block_index)
		block_mesh.surface_get_material(0).albedo_color = Color.transparent

func _ready() -> void:
	make_invisible()
