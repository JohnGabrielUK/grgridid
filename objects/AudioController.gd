extends Node

onready var audio_jumpin : AudioStreamPlayer = $Audio_JumpIn
onready var audio_jumpout : AudioStreamPlayer = $Audio_JumpOut
onready var audio_levelclear : AudioStreamPlayer = $Audio_LevelClear

onready var audio_bgm1 : AudioStreamPlayer = $Audio_BGM1

onready var tween : Tween = $Tween

onready var audios : Dictionary = {
	"jump_in": audio_jumpin,
	"jump_out": audio_jumpout,
	"level_clear": audio_levelclear
}

func play_sound(which : String) -> void:
	if audios.has(which):
		audios[which].play()
	else:
		printerr("play_sound was called with non-existant sound: " + which)

func change_bgm_scale(pitch_scale : float) -> void:
	tween.interpolate_property(audio_bgm1, "pitch_scale", null, pitch_scale, 0.1)
	tween.start()
