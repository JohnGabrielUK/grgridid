extends Spatial

onready var player : KinematicBody = $Player

func goal_reached() -> void:
	AudioController.play_sound("level_clear")
	yield(get_tree().create_timer(3.0), "timeout")
	Levels.next_level()

func _ready() -> void:
	player.connect("goal_reached", self, "goal_reached")
