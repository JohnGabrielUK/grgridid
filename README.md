# grGRIDid

## What?

An entry to the Ludum Dare 48 Compo.

## Who?

Me, JohnGabrielUK. The compo requires you do to pretty much everything yourself, so I did. About the only thing I didn't make was the font - it's Pixel Operator. And, of course, the game was made with the Godot Engine.

## Can I?

Probably - the game is licensed under GNU GPL v3. The art and music are [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
