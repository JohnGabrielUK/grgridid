extends Node

const LEVELS : Array = [
	"res://scenes/Level1.tscn",
	"res://scenes/Level2.tscn",
	"res://scenes/Level3.tscn",
	"res://scenes/Level4.tscn",
	"res://scenes/Level5.tscn"
]

var current_level : int
var start_time : int
var end_time : int

func new_game() -> void:
	start_time = OS.get_ticks_msec()
	current_level = 0
	load_level(0)

func next_level() -> void:
	current_level += 1
	if current_level == LEVELS.size():
		get_tree().change_scene("res://scenes/TheEnd.tscn")
		return
	load_level(current_level)

func load_level(which : int) -> void:
	var path : String = LEVELS[which]
	get_tree().change_scene(path)
